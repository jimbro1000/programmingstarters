# Standards

While this project is documentation rather than code it does follow 
some fundamental standards:

  1. Commit Messages: all commit messages **must** follow the 
  angular commit pattern
  
  2. Gitflow: gitflow branching is followed to separate new content
  from the "published" version
  
  3. Build Pipeline: the pipeline assembles a PDF document as
  a final output. It is the responsibility of the author to 
  ensure that the pipeline runs cleanly and produces the desired 
  output
  
  4. Semver: the project uses semantic versioning to document 
  changes
  
# Making Changes

  1. Start a new branch - add your new content/changes
  
  2. Make sure the commit messages meet angular commit standards
  
  3. If you are commit happy you only need one feat or fix commit,
  the rest can be chore. The commits will be flattened and the 
  feat/fix commit details retained for the changelog
  
  4. Ask for a merge to get your changes included in the main 
  branch
    
  5. Discuss your changes with the repo owners, get the changes 
  reviewed and in a happy, acceptable state

## Angular Commits

All commit messages must follow a set pattern:
  ```
  <type>(<scope>): <subject>
  <BLANK LINE>
  <body> 
  <BLANK LINE>
  <footer>
  ```

The type is one of:

  * 'feat' : feature
  * 'fix' : bug fix
  * 'doc' : documentation only
  * 'style' : style changes (format, white space)
  * 'refactor' : refactoring of code that neither fixes code or 
  add a feature
  * 'chore' : chore work
  
Type, scope and subject must all be lowercase or end with a period.

Subject must be in the imperative present tense. Use a short 
succinct description of  what the changes in the commit are for.

The body is also in the imperative present tense but has scope 
for more detail, context and motivation.

The footer should provide details of any breaking changes and any 
issues that the commit addresses.

The body and footer are both optional.
