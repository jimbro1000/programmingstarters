# Programming For Starters

This (brief) introduction to programming is aimed at everyone, not just beginners. 
It offers a fun, interactive, step-by-step companion to the basics of procedural 
programming covering the essential concepts that anyone can access*.

## What you Need

The guide assumes that you have access to a modern(ish) web browser and a working 
internet connection. These are not essential but will make it easier to follow 
and more fun. Beyond this you just need some time and a willingness to learn.

## Where to Start

The language used and the editor environment is completely web-based so there is 
need to download any software. The language goes by the name of "**Scratch**" and 
was created by the _Lifelong Kindergarten Group_ at the MIT Media Lab. While it is 
designed to be easily accessible for children it is not just for children. What 
it does provide is a language and editor that can be used by anyone regardless of 
experience and knowledge.

The Scratch language can be downloaded and run locally but these are not official 
products and tend to be different when compared to the MIT hosted Scratch site.

## What this Project Does

This project repository provides a mechanism for compiling and building a set of 
documentation for the lessons and any supporting files. If accessed through the 
git repository it forms an interactive document to aid the learning process.