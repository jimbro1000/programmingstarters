# Starters for Programming

While there are other approaches and views to computer programming, the vast
majority of code follows a single, simple approach called procedural 
programming. Under this bracket there are further sub-groups that offer 
variations on a theme, similarly each language seeks to add its own twist but 
under the covers they all follow the same approach and fundamental building 
blocks.

## Tools of the Trade

Regardless of the computer and the language every programming task requires a 
couple of common tools:

  * An editor
  * A compiler or interpreter
  
On top of these you need somewhere to run the code, either a computer or a 
virtual environment.

## A Common Language

Most languages are based around textual input - typing the individual 
syntactic components into a text editor or development environment. The
languages tend to either mnemonic or an idiosyncratic variation on the 
english language (other spoken languages have representation but tend to 
be quite rare by comparison).

The guide here uses something a little bit different, while it still relies 
on the english language, instead of typing each command this one uses a 
visual, graphical approach to the assembly of procedures. The language in 
question goes by the name of "**Scratch**" and while it was conceived to 
support teaching of programming in schools the approach has broad acceptance 
and positive reception at all levels and ages.

Scratch combines all of the common tools into a single browser based 
development environment that means there is nothing you need to install or 
download. You can access the scratch language at 
[https://scratch.mit.edu/create](https://scratch.mit.edu/create).

## The Integrated Development Environment (IDE)

Scratch is an interpreted language - meaning that when it runs it is the 
raw code that is processed. The alternative is a compiled language where 
the raw code is turned into a compact machine language that then runs 
natively on the computer.

The view when opening the scratch editor can seem a little intimidating 
but is just a set of dedicated panes in your browser window.

On the left is the command palette broken down into nine categories of 
commands. Next to it is the code window where you assemble your 
program.

To the right is a set of panes showing the output of your code at the top 
and the sprite and stage management panes below it.

At the top-left of the view is a set of three tabs - the first is labelled 
code and gives access to the default view. The second tab swaps the code 
panes for a sprite editor, the third presents an audio "editor" instead.

For the purposes of this guide we only need to worry about the code tab.